package com.example.ekern.mobileappsfinal_dae.dataaccess;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.ekern.mobileappsfinal_dae.MySQLiteHelper_User;

import com.example.ekern.mobileappsfinal_dae.MySQLiteHelper_User;

import com.example.ekern.mobileappsfinal_dae.models.User;

import java.util.ArrayList;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Created by ekern on 12/02/2017.
 */

public class UserGsonDataAccess {
    public static final String TAG = "UserGsonDataAccess";

    MySQLiteHelper_User dbHelper;
    SQLiteDatabase db;

    public static final String TABLE_NAME = "users";
    public static final String COLUMN_USER_ID = "_id";
    public static final String COLUMN_USER_GSON = "user_gson";

    public static final String TABLE_CREATE = String.format("create table %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT)", TABLE_NAME, COLUMN_USER_ID, COLUMN_USER_GSON);

    public UserGsonDataAccess(MySQLiteHelper_User dbHelper) {
        this.dbHelper = dbHelper;
        this.db = this.dbHelper.getWritableDatabase();
    }

    public User insertUser(User u) {
        ContentValues values = new ContentValues();

        values.put(COLUMN_USER_GSON, u.toJSON());
        long insertId = db.insert(TABLE_NAME, null, values);
        u.setId(insertId);
        return u;


    }

    public ArrayList<User> getAllUsers() {
        ArrayList<User> users = new ArrayList<User>();
        String query = String.format("SELECT %s, %s FROM %s", COLUMN_USER_ID, COLUMN_USER_GSON, TABLE_NAME);
        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {

            String gsonStringfromDb = c.getString(1);
            Log.i(TAG, gsonStringfromDb);
            User u = User.makeUserFromJSON(gsonStringfromDb);
//          String justUserJson = parse(gsonStringfromDb);
//
            String ustring = u.toString();

            u.setId(c.getLong(0));
            ustring = u.toString();
            Log.i(TAG, ustring);
            users.add(u);
            //the updateUsers call on the next line ensurers the the userID in the json string matches the __ID primary key
            updateUser(u);
            ustring = u.toString();
            Log.i(TAG, ustring);
            c.moveToNext();
        }
        c.close();
        return users;
    }

    public User updateUser(User u) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_ID, u.getId());
        values.put(COLUMN_USER_GSON, u.toJSON());

        int rowsUpdated = db.update(TABLE_NAME, values, "_id = " + u.getId(), null);
        return u;
    }

    public int deleteUser(User u) {
        int rowsDeleted = db.delete(TABLE_NAME, COLUMN_USER_ID + " = " + u.getId(), null);
        return rowsDeleted;
    }
}

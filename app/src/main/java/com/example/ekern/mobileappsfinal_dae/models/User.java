package com.example.ekern.mobileappsfinal_dae.models;

/**
 * Created by ekern on 11/27/2017.
 */


import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.Gson;

/**
 * Created by kadern on 9/18/2017.
 */

public class User implements Serializable {
    public enum Music {
        COUNTRY(1),
        RAP(2),
        JAZZ(3);

        private final int musicCode;

        Music(int musicCode) {
            this.musicCode = musicCode;
        }

        public int getmusicCode() {
            return this.musicCode;
        }




    }


    private String firstName;
    private String email;
    private Music favoriteMusic;
    private boolean active;
    //private boolean userValid=true;
    // private String validationMessage= "";
    private long id;
    //validUser used to keep track of validation state.
//    UserValidate validUser = new UserValidate(true, "");
    //these static vars are used to keep track of the validity of the user during instantiation
    public static boolean uValid = true;
    public static String uValidMessage = "";

    public User() {

    }

    public User(long id, String firstName, String email, Music favoriteMusic, boolean active) {
        if(uValid==true) {
            this.setId(id);
            this.setFirstName(firstName);
            this.setEmail(email);
            this.setFavoriteMusic(favoriteMusic);
            this.active = active;
        }



    }

    public long getId() {
        return id;
    }

    public void setId(long id) {


        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        if (firstName.equals("")) {
            uValid=false;
            uValidMessage = "Please enter user name";
        } else {

            this.firstName = firstName;
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        //user validated using regex function
      //  System.out.println("email valid? " +  User.validate(email) );

        if (!User.validate(email)) {

            uValid=false;
            uValidMessage += "\nPlease Enter A Valid email address";

        } else {
            this.email = email;
        }
    }

    public Music getFavoriteMusic() {
        return favoriteMusic;
    }

    public void setFavoriteMusic(Music favoriteMusic) {
        if (favoriteMusic == null) {


            uValid=false;
            uValidMessage += "\nPlease choose favorite music";

        } else {

            this.favoriteMusic = favoriteMusic;
        }
    }


    public static Music setMusicFromString(String s){
           s = s.toUpperCase();
        if(s=="COUNRTY"){
            return Music.COUNTRY;
        }else if(s=="JAZZ"){
            return Music.JAZZ;
        }else{
            return Music.RAP;
        }

    }



    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {

        this.active = active;
    }

    @Override
    public String toString() {
        return String.format("ID: %d FName: %s Email: %s Music: %s Active: %b", id, firstName, email, favoriteMusic, active);

    }

    public String toJSON() {
        Gson g = new Gson();


        String userJSON = g.toJson(this);
       Log.i("UserJsonString", userJSON);
        return userJSON;
    }

    public static User makeUserFromJSON(String jsonString) {
        //  Log.i("JSONSTING", jsonString);
        Gson g = new Gson();
        User u = g.fromJson(jsonString, User.class);
        //  Log.i("USERFROMJSON", u.toString());
        return u;

    }




    //function to validate email Source: https://stackoverflow.com/questions/8204680/java-regex-email

    private static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    private static boolean validate(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }


}


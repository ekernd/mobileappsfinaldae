package com.example.ekern.mobileappsfinal_dae;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.ekern.mobileappsfinal_dae.dataaccess.UserGsonDataAccess;
import com.example.ekern.mobileappsfinal_dae.dataaccess.AndroidDatabaseManager;
import com.example.ekern.mobileappsfinal_dae.models.User;

import java.util.ArrayList;

public class SQLiteActivity_User extends AppCompatActivity {

    public static final String TAG = "SQLiteActivity_User";
    MySQLiteHelper_User dbHelper;
    UserGsonDataAccess da;
    ListView usersListView;
    ArrayList<User> users = new ArrayList<>();
    Button btnAddUser;
    EditText txtUser;
    ArrayAdapter<User> adapter;
    User user;
    EditText txtFirstName;
    EditText txtEmail;
    RadioGroup rgFavoriteMusic;
    CheckBox chkActive;
    Button btnClearForm;
    Button btnDeleteUser;
    User userToDelete = new User();
    long userIdtoModify = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sqlite__user);
        usersListView = (ListView) findViewById(R.id.usersListView);
        txtFirstName = findViewById(R.id.txtFirstName);
        txtEmail = findViewById(R.id.txtEmail);
        rgFavoriteMusic = findViewById(R.id.rgFavoriteMusic);
        chkActive = findViewById(R.id.chkActive);
        btnDeleteUser = findViewById(R.id.btnDeleteUser);
        dbHelper = new MySQLiteHelper_User(this);
        da = new UserGsonDataAccess(dbHelper);   //pass in the connection link
        users = da.getAllUsers();

        adapter = new ArrayAdapter<User>(this, android.R.layout.simple_list_item_1, users);

        usersListView.setAdapter(adapter);

        //when a user in the user list is clicked upon, the user object data is put in the ui
        usersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User selectedUser = users.get(position);


                //this ccde sets the user to delete in the event the user selected from the list is deleted
                if (selectedUser != null) {
                    userToDelete.setId(selectedUser.getId());
                    userToDelete.setFirstName(selectedUser.getFirstName());
                    userToDelete.setEmail(selectedUser.getEmail());
                    userToDelete.setFavoriteMusic(selectedUser.getFavoriteMusic());
                    userToDelete.setActive(selectedUser.isActive());
                    //    Log.i("UserToDelete", userToDelete.toString());
                    putDataInUI(selectedUser);
                }

            }
        });

        //the info in the UI is cleared by this method
        btnClearForm = findViewById(R.id.btnClearForm);

        btnClearForm.setOnClickListener(new View.OnClickListener() {


                                            @Override
                                            public void onClick(View v) {


                                                resetForm();

                                            }
                                        }

        );

        //the user
        btnDeleteUser = findViewById(R.id.btnDeleteUser);
        //delete the user in the UI if the UI was populated from clicking on a user from the list
        btnDeleteUser.setOnClickListener(new View.OnClickListener() {


                                             @Override
                                             public void onClick(View v) {

                                                 deleteUser(userToDelete);


                                                 resetForm();
                                                 refreshUsers();
                                             }
                                         }


        );


        btnAddUser = findViewById(R.id.btnAddUser);

        //this code is what runs when the save button is pushed
        //a new user is added to the database or if a user was populated to the UI from the users list and then modified it is updated in the database
        btnAddUser.setOnClickListener(new View.OnClickListener() {


                                          @Override
                                          public void onClick(View v) {
                                              String msg = "";
                                              User newUser = null;
                                              newUser = getDataFromUI();
                                              Log.i("userfromform", newUser.toString());
                                              if (User.uValid) {
                                                  Log.i("add new user", newUser.toString());
                                                  //  Toast.makeText(SQLiteActivity_User.this, newUser.toString(), Toast.LENGTH_LONG).show();
                                                  if (newUser.getId() > 1) {
                                                      Log.i("add new user", newUser.toString());
                                                      da.updateUser(newUser);
                                                      refreshUsers();
                                                      resetForm();
                                                  } else {
                                                      da.insertUser(newUser);
                                                      Log.i("add new user", newUser.toString());
                                                      // Toast.makeText(SQLiteActivity_User.this, newUser.toString(), Toast.LENGTH_LONG).show();
                                                      refreshUsers();
                                                      resetForm();

                                                  }
                                                  //  txtUser.setText("");
                                              } else {
                                                  msg = User.uValidMessage;

                                                  Toast.makeText(SQLiteActivity_User.this, msg, Toast.LENGTH_LONG).show();
                                                  User.uValid = true;
                                                  User.uValidMessage = "";
                                              }

                                          }
                                      }


        );
    }

    //refresh user list
    private void refreshUsers() {
        adapter.clear();
        users = da.getAllUsers();
        adapter.addAll(users);
        adapter.notifyDataSetChanged();
        userIdtoModify = -1;

    }

    //creates a new user obj from the data entered into the ui.  If the user is new, the id will be set initially as "1".  If the user info was passed to the ui by clicking on an existing user in the user list, then the ID of the existing user will be returned by this method as well as the other user characteristic that appeared in the ui when the method was called.  This modified existing user can then be user to update the correct db entry.
    private User getDataFromUI() {
        long userID = 1;
        //the userIdtoModify variable is utilized to pass the ID of the user to the UI if the UI is populated by clicking on an existing user.

        //if the usertomodify is still set to -1 then the userID user in the return from this method will by 1.
        if (userIdtoModify > -1) {
            userID = userIdtoModify;
        }

        String firstName = txtFirstName.getText().toString();
        String email = txtEmail.getText().toString();
        boolean active = chkActive.isChecked();
        // Here's how we can set the favorite music...
        User.Music favoriteMusic = null;
        int selectedRadioButtonId = rgFavoriteMusic.getCheckedRadioButtonId();

        switch (selectedRadioButtonId) {
            case R.id.rbCountry:
                favoriteMusic = User.Music.COUNTRY;
                break;
            case R.id.rbRap:
                favoriteMusic = User.Music.RAP;
                break;
            case R.id.rbJazz:
                favoriteMusic = User.Music.JAZZ;
                break;
        }

        user = new User(userID, firstName, email, favoriteMusic, active);
        Log.d("User fromUI", user.toString());
        System.out.println("User fromUI" + user.toString());
        return user;
//        Toast.makeText(this, user.toString(), Toast.LENGTH_LONG).show();
    }

    private void putDataInUI(User user) {
        //when the UI is populated by clicking on an existing user in the list, the usertomodify user is used to pass the user ID along to other parameters because the userID itself is not a part of the UI
        userIdtoModify = user.getId();
        txtFirstName.setText(user.getFirstName());
        txtEmail.setText(user.getEmail());
        chkActive.setChecked(user.isActive());

        switch (user.getFavoriteMusic()) {
            case COUNTRY:
                rgFavoriteMusic.check(R.id.rbCountry);
                break;
            case JAZZ:
                rgFavoriteMusic.check(R.id.rbJazz);
                break;
            case RAP:
                rgFavoriteMusic.check(R.id.rbRap);
                break;
        }
    }

    //resets the UI
    private void resetForm() {
        rgFavoriteMusic.clearCheck();
        txtFirstName.setText("");
        txtEmail.setText("");
        chkActive.setChecked(false);
        userIdtoModify = -1;
    }


    private void deleteUser(User u) {
        Log.d("User to delete", u.toString());

        da.deleteUser(u);
        Toast.makeText(SQLiteActivity_User.this, "User deleted", Toast.LENGTH_LONG).show();


    }


}
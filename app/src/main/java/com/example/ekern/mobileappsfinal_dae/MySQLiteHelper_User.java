package com.example.ekern.mobileappsfinal_dae;

/**
 * Created by ekern on 11/27/2017.
 */


import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import com.example.ekern.mobileappsfinal_dae.dataaccess.UserGsonDataAccess;

import java.util.ArrayList;

/**
 * Created by ekern on 11/13/2017.
 */

public class MySQLiteHelper_User extends SQLiteOpenHelper {

    private static final String TAG = "MySQLiteHelper";
    private static final String DATA_BASE_NAME = "users.db";
    private static final int DATABASE_VERSION = 1;

    // Note that when the constructor gets called, Android will check to see
    // if the database file (samples.db) has been created.
    // It will invoke onCreate to create database if it has not already been created
    // If the database has already been created, then Android will
    // look at the DATABASE_VERSION param
    // and compare it to the the version number in samples.db
    // If the param (DATABASE_VERSION) is greater than the version number
    // in samples.db, then Android will call onUpgrade
    public MySQLiteHelper_User(Context context){
        super(context, DATA_BASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // create the 'items' table
        String sql = UserGsonDataAccess.TABLE_CREATE;
        db.execSQL(sql);
        Log.d(TAG, "creating db: " + sql);

        // if you needed other tables for your app, you would create them here as well
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // There is a strategy for upgrading the database, but it's complicated
        // if your app has already been distributed to users.
        // You have to be careful to upgrade the database so
        // that when users update your app, their data is not corrupted
    }

    public ArrayList<Cursor> getData(String Query){
        //get writable database
        SQLiteDatabase sqlDB = this.getWritableDatabase();
        String[] columns = new String[] { "message" };
        //an array list of cursor to save two cursors one has results from the query
        //other cursor stores error message if any errors are triggered
        ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
        MatrixCursor Cursor2= new MatrixCursor(columns);
        alc.add(null);
        alc.add(null);

        try{
            String maxQuery = Query ;
            //execute the query results will be save in Cursor c
            Cursor c = sqlDB.rawQuery(maxQuery, null);

            //add value to cursor2
            Cursor2.addRow(new Object[] { "Success" });

            alc.set(1,Cursor2);
            if (null != c && c.getCount() > 0) {

                alc.set(0,c);
                c.moveToFirst();

                return alc ;
            }
            return alc;
        } catch(SQLException sqlEx){
            Log.d("printing exception", sqlEx.getMessage());
            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[] { ""+sqlEx.getMessage() });
            alc.set(1,Cursor2);
            return alc;
        } catch(Exception ex){
            Log.d("printing exception", ex.getMessage());

            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[] { ""+ex.getMessage() });
            alc.set(1,Cursor2);
            return alc;
        }
    }




}

package com.example.ekern.mobileappsfinal_dae;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.ekern.mobileappsfinal_dae.dataaccess.AndroidDatabaseManager;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_options_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.users:
                startActivity(new Intent(this, SQLiteActivity_User.class));
                return true;

            case R.id.usersInDb:
                startActivity(new Intent(this, AndroidDatabaseManager.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}

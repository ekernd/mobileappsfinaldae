package com.example.ekern.mobileappsfinal_dae.models;

/**
 * Created by ekern on 12/02/2017.
 */

public class UserTester {
    public static void main(String[] args) {
        // TODO Auto-generated method stub

        //Example:  below we will create an array of JSON dog
        // / /strings. then use Gson to convert the JSON string array   to JAVA Dog objects

        String jSonStringDog1 = "{\"dogName\":\"Jeff\", \"dogDateOfBirth\":{\"year\":2016, \"month\": 3, \"date\": 12}, \"dogWeight\":54, \"dogBreed\":\"Jack Russel Terrier\", \"sterile\":\"false\"}";
        System.out.println(jSonStringDog1);
//        String jSonStringDog2 = "{\"dogName\":\"Bucky\", \"dogDateOfBirth\":{\"year\":2011, \"month\": 3, \"date\": 21}, \"dogWeight\":140, \"dogBreed\":\"Moose Hound\", \"sterile\":\"false\"}";
//
//        String jSonStringDog3 = "{\"dogName\":\"Spike\", \"dogDateOfBirth\":{\"year\":2017, \"month\": 10, \"date\": 12}, \"dogWeight\":2, \"dogBreed\":\"Toy Fox Terrier\", \"sterile\":\"false\"}";
//
//        String jSonStringDog4 = "{\"dogName\":\"Mrs. Monkey\", \"dogDateOfBirth\":{\"year\":2013, \"month\": 9, \"date\": 27}, \"dogWeight\":47, \"dogBreed\":\"Golden Retriever\", \"sterile\":\"true\"}";
//
//        String jSonStringDog5 = "{\"dogName\":\"Gitdog\", \"dogDateOfBirth\":{\"year\":2005, \"month\": 7, \"date\": 4}, \"dogWeight\":17, \"dogBreed\":\"mutt\", \"sterile\":\"true\"}";
//
//
//        String jsonArrayOfDogs ="["+ jSonStringDog1+","+ jSonStringDog2+","+jSonStringDog3+","+jSonStringDog4+","+jSonStringDog5 + "]";
//        System.out.println(jsonArrayOfDogs);
//        Gson g = new Gson();
//        Dog[] arrOfDogObjects = g.fromJson(jsonArrayOfDogs, Dog[].class);
//
//        for (Dog d:arrOfDogObjects) {
//            System.out.println(d.toString());
//
//        }
//
//        //Examples:
//        Dog chloe = new Dog("Chloe", (new Date(2013, 8, 28)), 27, "Feist", true);
//
//        //    //from JAVA obj to json string
//        String chloeJsonStr = g.toJson(chloe);
//        //
//        String jSonStringDog = "{\"dogName\":\"John\", \"dogDateOfBirth\":{\"year\":2016, \"month\": 3, \"date\": 12}, \"dogWeight\":54, \"dogBreed\":\"Jack Russel Terrier\", \"sterile\":\"false\"}";
//        //
//          //fron json string to java object
//        Dog john = g.fromJson(jSonStringDog, Dog.class);

        //  	User u = new User("dave", "ekernd@gmail.com", Music.valu )


        User.Music arr[] = User.Music.values();
        for (User.Music mus: arr) {
            System.out.println(mus + " at "+ mus.ordinal());
        }


//        User u = new User(1,"dave","ek@r.com", User.setMusicFromString("jazz"), true );
//        System.out.println( u.toString() );
//        System.out.println( User.uValid);
//        System.out.println( User.uValidMessage);
//        System.out.println( u.toJSON() );

        String uString = "{\"active\":false,\"email\":\"thibg@yyf.vyf\",\"favoriteMusic\":\"JAZZ\",\"firstName\":\"yihbfgjnb\",\"id\":1}";

        User us = User.makeUserFromJSON(uString);
        System.out.println(us.toString());

    }
}
